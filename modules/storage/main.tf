# create an s3 bucket
 
resource "aws_s3_bucket" "terraform-state-file" {
  bucket = "${var.bucket_name}"
  versioning {enabled = true}
  
 tags {
   Name = "S3 Remote Bucket for Terraform State Store"
   Environment = "${var.environment}"

 } 	

}

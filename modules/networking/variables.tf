########### Variables file################################################
# This file defines all the variables and interfaces  needed for the module
##########################################################################

############## Create two subnets################
# 1 subnet for public access for Elastic LB
# 1 subnet for private access for 2 web servers


variable "vpc_cidr" {
  description = "The CIDR block of the VPC"
} 

variable "public_subnet_cidr" {
  description = "The CIDR block for the public subnet"
}

variable "private_subnet_cidr" {
  description = "The CIDR block for the private subnet"
}

variable "environment" {
  description = "Environment"
}

variable "region" {
  description = "The region to launch the bastion host"
}

variable "availability_zone" {
  description = "The az that the resources will be launched"
}

##### Create a bastion host for accssing the web servers via ssh
variable "bastion_ami" {
  default = {
    "us-east-1" = "ami-009d6802948d06e52"
    "us-east-2" = "ami-02e680c4540db351e"
  }
}

variable "key_name" {
  description = "The public key for the bastion host"
}
